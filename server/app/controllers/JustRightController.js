var schedule = require('node-schedule');

let DEBUG = false;

class DirectionComponent {
	constructor(value, units) {
		this.value = value;
		this.units = units;
		this.target = value;
	}

	randomise(value) {
		this.value = +(value + (Math.random() * 10)).toFixed(0)
	}
}

class Vote {
	constructor(vote) {
		this.userId = vote.userId;
		if (vote.type == 0)
			this.tempTarget = vote.value;
		if (vote.type == 1)
			this.brightTarget = vote.value;
	}
}

function log(...message) {
	let time = new Date().toLocaleTimeString();
	for (let i = 0; i < message.length; i++) {
		console.log(time + ": " + message[i]);
	}
}

const credentials = {
	client: {
		id: '36f86b60-47f0-48bf-98e1-5f9bcca7cc8d',
		secret: '4SeWUp3vay058ku1aBrkx1FSU'
	},
	auth: {
		tokenHost: 'https://api.home.nest.com/oauth2/access_token',
		authUrl: "https://home.nest.com/login/oauth2?client_id=36f86b60-47f0-48bf-98e1-5f9bcca7cc8d&state=STATE"
	}
};

exports.JustRightController = class JustRightController {
	constructor() {
		this.votes = [];

		this.temperature = new DirectionComponent(0, '°C');
		this.brightness = new DirectionComponent(0, 'lm');

		this.temperature.randomise(15);
		this.brightness.randomise(40);

		this.recalculate();
		this.recalculator = schedule.scheduleJob('*/15 * * * *', this.recalculate.bind(this));
	}

	getDetails() {
		return {
			temperature: this.temperature,
			brightness: this.brightness
		}
	}

	addVote(req, res) {
		try {
			this.votes.push(new Vote(req.body));

			res.json({
				accepted: true
			});
		} catch (ex) {
			res.status(400).json({
				accepted: false
			});
		}
	}

	recalculate() {
		log("Recalculating temperature and brightness targets");
		var counts = {
			temperature: {
				"0": 0,
				"1": 0,
				"-1": 0
			},
			brightness: {
				"0": 0,
				"1": 0,
				"-1": 0
			}
		};
		this.votes.forEach(vote => {
			switch (vote.tempTarget) {
				case -1:
					counts.temperature["-1"]++;
					break;
				case 0:
					counts.temperature["0"]++;
					break;
				case 1:
					counts.temperature["1"]++;
					break;
			}

			switch (vote.brightTarget) {
				case -1:
					counts.brightness["-1"]++;
					break;
				case 0:
					counts.brightness["0"]++;
					break;
				case 1:
					counts.brightness["1"]++;
					break;
			}
		});
		var tempDiff = 0,
			brightDiff = 0;
		if (counts.temperature["1"] != 0 || counts.temperature["-1"] != 0 || counts.temperature["0"] != 0)
			tempDiff = Object.keys(counts.temperature).reduce((a, b) => counts.temperature[a] > counts.temperature[b] ? a : b);
		if (counts.brightness["1"] != 0 || counts.brightness["-1"] != 0 || counts.brightness["0"] != 0)
			brightDiff = Object.keys(counts.brightness).reduce((a, b) => counts.brightness[a] > counts.brightness[b] ? a : b);

		this.temperature.target = this.temperature.value + parseInt(tempDiff);
		this.brightness.target = this.brightness.value + parseInt(brightDiff);

		this.votes.length = 0;

		log("Changing temperature: " + this.temperature.value + this.temperature.units + " => " + this.temperature.target + this.temperature.units);
		log("Changing brightness: " + this.brightness.value + this.brightness.units + " => " + this.brightness.target + this.brightness.units);
	}

	authorise(req, res) {
		log(this.auth_url);
		var auth_code = "NA36G5EP";
		var qs = require("querystring");
		var http = require("https");

		var options = {
			"method": "POST",
			"hostname": "api.home.nest.com",
			"port": null,
			"path": "/oauth2/access_token",
			"headers": {
				"content-type": "application/x-www-form-urlencoded"
			}
		};

		var req = http.request(options, function (res) {
			var chunks = [];

			res.on("data", function (chunk) {
				chunks.push(chunk);
			});

			res.on("end", function () {
				var body = Buffer.concat(chunks);
				log(body.toString());
			});
		});

		req.write(qs.stringify({
			code: 'AUTH_CODE',
			client_id: 'CLIENT_ID',
			client_secret: 'CLIENT_SECRET',
			grant_type: 'authorization_code'
		}));
		req.end();
	}
}
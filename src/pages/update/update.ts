import { Component } from '@angular/core';
import { Events, Refresher } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';

let graph_directions: Array<string> = ['arrow-up', 'arrow-down', 'git-commit'];
let graph_colours: Array<string> = ['has-text-danger', 'has-text-info', 'has-text-success'];

let DEBUG = false;

class DirectionComponent {
	colour: string;
	direction: string;
	value: number;
	target: number;
	units: string;

	constructor(units: string) {
		this.units = units;
	}

	setValue(value: number, units?: string) {
		this.value = value;
		this.update();
	}

	setTarget(value: number, units?: string) {
		this.target = value;
		this.update();
	}

	update() {
		this.colour = this.target > this.value ? graph_colours[0] : this.target < this.value ? graph_colours[1] : graph_colours[2];
		this.direction = this.target > this.value ? graph_directions[0] : this.target < this.value ? graph_directions[1] : graph_directions[2];
	}
}

@Component({
	selector: 'page-update',
	templateUrl: 'update.html'
})
export class UpdatePage {
	url: string = DEBUG ? 'http://localhost:9090/api/v1' : 'http://justright.anooserve.com:9090/api/v1';

	temperature: DirectionComponent;
	brightness: DirectionComponent;
	serverDetailsTimer: number;
	canVote: Array<boolean> = [true, true];
	voteTime: Array<Date> = [new Date(), new Date()];

	constructor(public navCtrl: NavController, public http: Http, public events: Events, public toastCtrl: ToastController) {
		this.temperature = new DirectionComponent('°C');
		this.brightness = new DirectionComponent('lm');

		this.getServerDetails();
		// Update every 30 seconds automatically
		this.serverDetailsTimer = setInterval(this.getServerDetails.bind(this), 1000 * 30);
	}

	postRequest(type: number, value: number) {
		if (!this.canVote[type]) {
			this.presentToast("You may vote again in " + Math.floor(Math.abs((this.voteTime[type].getTime() - new Date().getTime()) / 1000)) + "s");
			return;
		}

		this.http.post(this.url + '/vote', { type: type, value: value }).subscribe(resp => {
			resp["time"] = new Date().toLocaleTimeString();
			this.events.publish('server-data', resp);
			this.presentToast("Vote Accepted");
			this.canVote[type] = false;
			this.voteTime[type] = new Date(new Date().getTime() + (5 * 60 * 1000));
			// You can vote again in 5 minutes
			setInterval(()=> {this.canVote.fill(true);}, 1000 * 60 * 5);
		}, error => {
			console.error("Failed to post to server", error);
			this.events.publish('server-data', error);
			this.presentToast("Vote Rejected");
		});
	}

	getServerDetails(refresher?: Refresher) {
		this.http.get(this.url + '/details').subscribe(resp => {
			var response = resp.json();
			console.log(response);

			this.temperature.setTarget(response.temperature.target, response.temperature.units);
			this.temperature.setValue(response.temperature.value, response.temperature.units);

			this.brightness.setTarget(response.brightness.target, response.brightness.units);
			this.brightness.setValue(response.brightness.value, response.brightness.units);

			resp["time"] = new Date().toLocaleTimeString();
			this.events.publish('server-data', resp);
		}, error => {
			console.error("Failed to get from server", error);
			this.events.publish('server-data', error);
		}, () => {
			if (refresher)
				refresher.complete();
		});
	}

	presentToast(message) {
		let toast = this.toastCtrl.create({
			message: message || "empty toast",
			duration: 3000
		});
		toast.present();
	}
}

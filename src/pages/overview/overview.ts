import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { NavController } from 'ionic-angular';

@Component({
	selector: 'page-overview',
	templateUrl: 'overview.html'
})
export class OverviewPage {
	serverData = [];

	constructor(public navCtrl: NavController, public events: Events) {
		events.subscribe('server-data', data => {
			this.serverData.push([data.time, JSON.stringify(data, null, 2)]);
		});
	}
}
